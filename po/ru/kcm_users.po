# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2020, 2021, 2022, 2023 Alexander Yavorsky <kekcuha@gmail.com>
# Alexander Potashev <aspotashev@gmail.com>, 2020.
# Дронова Ю <juliette.tux@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-25 00:38+0000\n"
"PO-Revision-Date: 2023-12-07 22:36+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.3\n"

#: src/fingerprintmodel.cpp:151 src/fingerprintmodel.cpp:258
#, kde-format
msgid "No fingerprint device found."
msgstr "Устройство для считывания отпечатков пальцев не обнаружено."

#: src/fingerprintmodel.cpp:331
#, kde-format
msgid "Retry scanning your finger."
msgstr "Повторите считывание отпечатка пальца."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Очень короткое перемещение пальца, попробуйте ещё раз."

#: src/fingerprintmodel.cpp:335
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Палец не расположен над центром считывателя, попробуйте ещё раз."

#: src/fingerprintmodel.cpp:337
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Уберите палец со считывателя и попробуйте ещё раз."

#: src/fingerprintmodel.cpp:345
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Не удалось считать отпечаток пальца."

#: src/fingerprintmodel.cpp:348
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"В памяти устройстве закончилось место для хранения отпечатков пальцев, для "
"продолжения удалите сохранённые отпечатки."

#: src/fingerprintmodel.cpp:351
#, kde-format
msgid "The device was disconnected."
msgstr "Устройство отключено."

#: src/fingerprintmodel.cpp:356
#, kde-format
msgid "An unknown error has occurred."
msgstr "Произошла неизвестная ошибка."

#: src/ui/ChangePassword.qml:27 src/ui/UserDetailsPage.qml:170
#, kde-format
msgid "Change Password"
msgstr "Изменить пароль"

#: src/ui/ChangePassword.qml:32
#, kde-format
msgid "Set Password"
msgstr "Задать пароль"

#: src/ui/ChangePassword.qml:55
#, kde-format
msgid "Password"
msgstr "Пароль"

#: src/ui/ChangePassword.qml:70
#, kde-format
msgid "Confirm password"
msgstr "Подтвердите пароль"

#: src/ui/ChangePassword.qml:89 src/ui/CreateUser.qml:68
#, kde-format
msgid "Passwords must match"
msgstr "Пароли должны совпадать"

#: src/ui/ChangeWalletPassword.qml:16
#, kde-format
msgid "Change Wallet Password?"
msgstr "Изменить пароль бумажника (хранителя паролей)?"

#: src/ui/ChangeWalletPassword.qml:25
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"После смены пароля входа в систему также можно сменить пароль хранителя "
"паролей KWallet по умолчанию, так, чтобы пароли совпадали."

#: src/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "Что такое KWallet?"

#: src/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet — это программа управления паролями, которая хранит пароли от "
"беспроводной сети и других зашифрованных ресурсов. Эта программа защищена "
"своим собственным паролем, который отличается от пароля пользователя, "
"используемого для входа в систему. Если оба эти пароля будут совпадать, то "
"KWallet будет разблокирован автоматически при входе пользователя в систему, "
"без дополнительного ввода пароля вручную."

#: src/ui/ChangeWalletPassword.qml:58
#, kde-format
msgid "Change Wallet Password"
msgstr "Изменить пароль бумажника (хранителя паролей)"

#: src/ui/ChangeWalletPassword.qml:67
#, kde-format
msgid "Leave Unchanged"
msgstr "Оставить без изменений"

#: src/ui/CreateUser.qml:15
#, kde-format
msgid "Create User"
msgstr "Создание пользователя"

#: src/ui/CreateUser.qml:33 src/ui/UserDetailsPage.qml:132
#, kde-format
msgid "Name:"
msgstr "Имя:"

#: src/ui/CreateUser.qml:37 src/ui/UserDetailsPage.qml:140
#, kde-format
msgid "Username:"
msgstr "Имя пользователя:"

#: src/ui/CreateUser.qml:47 src/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Standard"
msgstr "Обычный пользователь"

#: src/ui/CreateUser.qml:48 src/ui/UserDetailsPage.qml:151
#, kde-format
msgid "Administrator"
msgstr "Администратор"

#: src/ui/CreateUser.qml:51 src/ui/UserDetailsPage.qml:154
#, kde-format
msgid "Account type:"
msgstr "Тип учётной записи:"

#: src/ui/CreateUser.qml:56
#, kde-format
msgid "Password:"
msgstr "Пароль:"

#: src/ui/CreateUser.qml:61
#, kde-format
msgid "Confirm password:"
msgstr "Подтверждение пароля:"

#: src/ui/CreateUser.qml:78
#, kde-format
msgid "Create"
msgstr "Создать"

#: src/ui/FingerprintDialog.qml:43
#, kde-format
msgid "Configure Fingerprints"
msgstr "Настроить использование отпечатков пальцев"

#: src/ui/FingerprintDialog.qml:52
#, kde-format
msgctxt "@action:button 'all' refers to fingerprints"
msgid "Clear All"
msgstr "Очистить все"

#: src/ui/FingerprintDialog.qml:59
#, kde-format
msgid "Add"
msgstr "Добавить"

#: src/ui/FingerprintDialog.qml:68
#, kde-format
msgid "Cancel"
msgstr "Отмена"

#: src/ui/FingerprintDialog.qml:76
#, kde-format
msgid "Done"
msgstr "Готово"

#: src/ui/FingerprintDialog.qml:100
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Считывание отпечатка пальца"

#: src/ui/FingerprintDialog.qml:110
#, kde-format
msgid ""
"Please repeatedly press your right index finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка указательного пальца правой руки."

#: src/ui/FingerprintDialog.qml:112
#, kde-format
msgid ""
"Please repeatedly press your right middle finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка среднего пальца правой руки."

#: src/ui/FingerprintDialog.qml:114
#, kde-format
msgid ""
"Please repeatedly press your right ring finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка безымянного пальца правой руки."

#: src/ui/FingerprintDialog.qml:116
#, kde-format
msgid ""
"Please repeatedly press your right little finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка мизинца правой руки."

#: src/ui/FingerprintDialog.qml:118
#, kde-format
msgid "Please repeatedly press your right thumb on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка большого пальца правой руки."

#: src/ui/FingerprintDialog.qml:120
#, kde-format
msgid ""
"Please repeatedly press your left index finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка указательного пальца левой руки."

#: src/ui/FingerprintDialog.qml:122
#, kde-format
msgid ""
"Please repeatedly press your left middle finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка среднего пальца левой руки."

#: src/ui/FingerprintDialog.qml:124
#, kde-format
msgid ""
"Please repeatedly press your left ring finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка безымянного пальца левой руки."

#: src/ui/FingerprintDialog.qml:126
#, kde-format
msgid ""
"Please repeatedly press your left little finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка мизинца левой руки."

#: src/ui/FingerprintDialog.qml:128
#, kde-format
msgid "Please repeatedly press your left thumb on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка большого большого левой руки."

#: src/ui/FingerprintDialog.qml:132
#, kde-format
msgid ""
"Please repeatedly swipe your right index finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка указательного пальца правой руки."

#: src/ui/FingerprintDialog.qml:134
#, kde-format
msgid ""
"Please repeatedly swipe your right middle finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка среднего пальца правой руки."

#: src/ui/FingerprintDialog.qml:136
#, kde-format
msgid ""
"Please repeatedly swipe your right ring finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка безымянного пальца правой руки."

#: src/ui/FingerprintDialog.qml:138
#, kde-format
msgid ""
"Please repeatedly swipe your right little finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка мизинца правой руки."

#: src/ui/FingerprintDialog.qml:140
#, kde-format
msgid "Please repeatedly swipe your right thumb on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка большого пальца правой руки."

#: src/ui/FingerprintDialog.qml:142
#, kde-format
msgid ""
"Please repeatedly swipe your left index finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка указательного пальца левой руки."

#: src/ui/FingerprintDialog.qml:144
#, kde-format
msgid ""
"Please repeatedly swipe your left middle finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка среднего пальца левой руки."

#: src/ui/FingerprintDialog.qml:146
#, kde-format
msgid ""
"Please repeatedly swipe your left ring finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка безымянного пальца левой руки."

#: src/ui/FingerprintDialog.qml:148
#, kde-format
msgid ""
"Please repeatedly swipe your left little finger on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка мизинца левой руки."

#: src/ui/FingerprintDialog.qml:150
#, kde-format
msgid "Please repeatedly swipe your left thumb on the fingerprint sensor."
msgstr "Повторяйте считывание отпечатка большого большого левой руки."

#: src/ui/FingerprintDialog.qml:166
#, kde-format
msgid "Finger Enrolled"
msgstr "Отпечаток пальца получен"

#: src/ui/FingerprintDialog.qml:198
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Выберите палец для считывания отпечатка"

#: src/ui/FingerprintDialog.qml:318
#, kde-format
msgid "Re-enroll finger"
msgstr "Повторно считать отпечаток пальца"

#: src/ui/FingerprintDialog.qml:325
#, kde-format
msgid "Delete fingerprint"
msgstr "Удалить отпечаток пальца"

#: src/ui/FingerprintDialog.qml:334
#, kde-format
msgid "No fingerprints added"
msgstr "Нет ни одного сохранённого отпечатка пальца"

#: src/ui/main.qml:20
#, kde-format
msgid "Users"
msgstr "Пользователи"

#: src/ui/main.qml:31
#, kde-format
msgctxt "@action:button As in, 'add new user'"
msgid "Add New"
msgstr "Добавить пользователя"

#: src/ui/main.qml:107
#, kde-format
msgctxt "@info:usagetip"
msgid "Press Space to edit the user profile"
msgstr "Для редактирования профиля нажмите пробел"

#: src/ui/PicturesSheet.qml:18
#, kde-format
msgctxt "@title"
msgid "Change Avatar"
msgstr "Изменение аватара"

#: src/ui/PicturesSheet.qml:22
#, kde-format
msgctxt "@item:intable"
msgid "It's Nothing"
msgstr "Не использовать"

#: src/ui/PicturesSheet.qml:23
#, kde-format
msgctxt "@item:intable"
msgid "Feisty Flamingo"
msgstr "Энергичный фламинго"

#: src/ui/PicturesSheet.qml:24
#, kde-format
msgctxt "@item:intable"
msgid "Dragon's Fruit"
msgstr "Драконий фрукт"

#: src/ui/PicturesSheet.qml:25
#, kde-format
msgctxt "@item:intable"
msgid "Sweet Potato"
msgstr "Сладкий картофель"

#: src/ui/PicturesSheet.qml:26
#, kde-format
msgctxt "@item:intable"
msgid "Ambient Amber"
msgstr "Светящийся янтарь"

#: src/ui/PicturesSheet.qml:27
#, kde-format
msgctxt "@item:intable"
msgid "Sparkle Sunbeam"
msgstr "Блестящий солнечный луч"

#: src/ui/PicturesSheet.qml:28
#, kde-format
msgctxt "@item:intable"
msgid "Lemon-Lime"
msgstr "Лимонно-лаймовый"

#: src/ui/PicturesSheet.qml:29
#, kde-format
msgctxt "@item:intable"
msgid "Verdant Charm"
msgstr "Зеленеющее очарование"

#: src/ui/PicturesSheet.qml:30
#, kde-format
msgctxt "@item:intable"
msgid "Mellow Meadow"
msgstr "Густой луг"

#: src/ui/PicturesSheet.qml:31
#, kde-format
msgctxt "@item:intable"
msgid "Tepid Teal"
msgstr "Тёплый чирок"

#: src/ui/PicturesSheet.qml:32
#, kde-format
msgctxt "@item:intable"
msgid "Plasma Blue"
msgstr "Синяя плазма"

# color names; hints: https://marc.info/?l=kde-i18n-doc&m=159355931105508&w=2 --aspotashev
#: src/ui/PicturesSheet.qml:33
#, kde-format
msgctxt "@item:intable"
msgid "Pon Purple"
msgstr "Фиолетовый помпон"

# color names; hints: https://marc.info/?l=kde-i18n-doc&m=159355931105508&w=2 --aspotashev
#: src/ui/PicturesSheet.qml:34
#, kde-format
msgctxt "@item:intable"
msgid "Bajo Purple"
msgstr "Фиолетовый бахо"

#: src/ui/PicturesSheet.qml:35
#, kde-format
msgctxt "@item:intable"
msgid "Burnt Charcoal"
msgstr "Обожжённый уголь"

#: src/ui/PicturesSheet.qml:36
#, kde-format
msgctxt "@item:intable"
msgid "Paper Perfection"
msgstr "Бумажное совершенство"

# color names; hints: https://marc.info/?l=kde-i18n-doc&m=159355931105508&w=2 --aspotashev
#: src/ui/PicturesSheet.qml:37
#, kde-format
msgctxt "@item:intable"
msgid "Cafétera Brown"
msgstr "Чёрный кофе"

# color names; hints: https://marc.info/?l=kde-i18n-doc&m=159355931105508&w=2 --aspotashev
#: src/ui/PicturesSheet.qml:38
#, kde-format
msgctxt "@item:intable"
msgid "Rich Hardwood"
msgstr "Паркет из твёрдого дерева"

#: src/ui/PicturesSheet.qml:60
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr "Назад"

#: src/ui/PicturesSheet.qml:78
#, kde-format
msgctxt "@action:button"
msgid "Initials"
msgstr "Инициалы"

#: src/ui/PicturesSheet.qml:130
#, kde-format
msgctxt "@action:button"
msgid "Choose File…"
msgstr "Выбрать файл…"

#: src/ui/PicturesSheet.qml:135
#, kde-format
msgctxt "@title"
msgid "Choose a picture"
msgstr "Выбор изображения"

#: src/ui/PicturesSheet.qml:183
#, kde-format
msgctxt "@action:button"
msgid "Placeholder Icon"
msgstr ""

#: src/ui/PicturesSheet.qml:271
#, kde-format
msgctxt "@info:whatsthis"
msgid "User avatar placeholder icon"
msgstr ""

#: src/ui/UserDetailsPage.qml:111
#, kde-format
msgid "Change avatar"
msgstr "Изменить аватар"

#: src/ui/UserDetailsPage.qml:164
#, kde-format
msgid "Email address:"
msgstr "Адрес эл. почты:"

#: src/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Delete files"
msgstr "Удалить файлы"

#: src/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Keep files"
msgstr "Оставить файлы"

#: src/ui/UserDetailsPage.qml:207
#, kde-format
msgid "Delete User…"
msgstr "Удалить пользователя..."

#: src/ui/UserDetailsPage.qml:219
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Настроить аутентификацию по отпечатку пальца…"

#: src/ui/UserDetailsPage.qml:248
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Отпечатки пальцев могут использоваться вместо ввода пароля для "
"разблокирования экрана и подтверждения повышения прав доступа при "
"использовании приложений или при работе в командной строке.<nl/><nl/"
">Использование отпечатков пальцев для входа в систему пока не поддерживается."

#: src/user.cpp:291
#, kde-format
msgid "Could not get permission to save user %1"
msgstr ""
"Не удалось получить требуемые разрешения для сохранения параметров "
"пользователя %1"

#: src/user.cpp:296
#, kde-format
msgid "There was an error while saving changes"
msgstr "Ошибка сохранения изменений"

#: src/user.cpp:395
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""
"Не удалось изменить размер изображения: не удалось открыть временный файл"

#: src/user.cpp:403
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr ""
"Не удалось изменить размер изображения: не удалось записать временный файл"

#: src/usermodel.cpp:147
#, kde-format
msgid "Your Account"
msgstr "Ваша учётная запись"

#: src/usermodel.cpp:147
#, kde-format
msgid "Other Accounts"
msgstr "Другие учётные записи"

#~ msgid "Manage Users"
#~ msgstr "Управление пользователями"

#~ msgid "Continue"
#~ msgstr "Продолжить"

#~ msgid "Please repeatedly "
#~ msgstr "Повторяйте считывание несколько раз "

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "ivan.ivanov@example.com"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Александр Яворский,Дронова Юлия"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kekcuha@gmail.com"

#~ msgid "Manage user accounts"
#~ msgstr "Управление учётными записями пользователей"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgid "Devin Lin"
#~ msgstr "Devin Lin"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "Иван Иванов"
