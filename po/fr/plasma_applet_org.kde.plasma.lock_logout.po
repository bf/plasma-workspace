# translation of plasma_applet_lockout.po to Français
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Nicolas Ternisien <nicolas.ternisien@gmail.com>, 2009.
# Joëlle Cornavin <jcorn@free.fr>, 2009, 2010, 2012, 2013.
# Xavier Besnard <xavier.besnard@kde.org>, 2013.
# Thomas Vergnaud <thomas.vergnaud@gmx.fr>, 2015.
# Simon Depiets <sdepiets@gmail.com>, 2019.
# SPDX-FileCopyrightText: 2020, 2021, 2023 Xavier Besnard <xavier.besnard@kde.org>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_lockout\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-19 01:53+0000\n"
"PO-Revision-Date: 2023-10-21 12:56+0200\n"
"Last-Translator: Xavier BESNARD <xavier.besnard@neuf.fr>\n"
"Language-Team: fr\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.08.1\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Général"

#: contents/ui/ConfigGeneral.qml:33
#, kde-format
msgctxt ""
"Heading for a list of actions (leave, lock, switch user, hibernate, suspend)"
msgid "Show actions:"
msgstr "Afficher les actions :"

#: contents/ui/ConfigGeneral.qml:34
#, kde-format
msgid "Log Out"
msgstr "Se déconnecter"

#: contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Shut Down"
msgstr "Éteindre"

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Restart"
msgstr "Redémarrer"

#: contents/ui/ConfigGeneral.qml:53 contents/ui/data.js:5
#, kde-format
msgid "Lock"
msgstr "Verrouiller"

#: contents/ui/ConfigGeneral.qml:59
#, kde-format
msgid "Switch User"
msgstr "Changer d'utilisateur"

#: contents/ui/ConfigGeneral.qml:65 contents/ui/data.js:46
#, kde-format
msgid "Hibernate"
msgstr "Veille prolongée"

#: contents/ui/ConfigGeneral.qml:71 contents/ui/data.js:39
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Veille"

#: contents/ui/data.js:6
#, kde-format
msgid "Lock the screen"
msgstr "Verrouiller l'écran"

#: contents/ui/data.js:12
#, kde-format
msgid "Switch user"
msgstr "Changer d'utilisateur"

#: contents/ui/data.js:13
#, kde-format
msgid "Start a parallel session as a different user"
msgstr "Démarre une session parallèle en tant qu'utilisateur différent"

#: contents/ui/data.js:18
#, kde-format
msgid "Shutdown…"
msgstr "Éteindre..."

#: contents/ui/data.js:19
#, kde-format
msgid "Turn off the computer"
msgstr "Éteindre l'ordinateur"

#: contents/ui/data.js:25
#, kde-format
msgid "Restart…"
msgstr "Redémarrer..."

#: contents/ui/data.js:26
#, kde-format
msgid "Reboot the computer"
msgstr "Re-démarrer l'ordinateur"

#: contents/ui/data.js:32
#, kde-format
msgid "Logout…"
msgstr "Déconnexion..."

#: contents/ui/data.js:33
#, kde-format
msgid "End the session"
msgstr "Terminer la session"

#: contents/ui/data.js:40
#, kde-format
msgid "Sleep (suspend to RAM)"
msgstr "Veille (mise en veille)"

#: contents/ui/data.js:47
#, kde-format
msgid "Hibernate (suspend to disk)"
msgstr "Veille prolongée (mettre en veille prolongée)"

#~ msgid "Logout"
#~ msgstr "Déconnexion"

#~ msgid "Reboot"
#~ msgstr "Re-démarrer"

#~ msgid "Shutdown..."
#~ msgstr "Éteindre..."

#~ msgid "Logout..."
#~ msgstr "Déconnexion..."

#~ msgid "Do you want to suspend to disk (hibernate)?"
#~ msgstr "Voulez-vous passer en veille prolongée (mode hibernation) ?"

#~ msgid "Yes"
#~ msgstr "Oui"

#~ msgid "No"
#~ msgstr "Non"

#~ msgid "Do you want to suspend to RAM (sleep)?"
#~ msgstr "Voulez-vous passer en veille (mode veille) ?"

#~ msgid "Leave"
#~ msgstr "Quitter"

#~ msgid "Leave..."
#~ msgstr "Quitter..."

#~ msgctxt "Heading for list of actions (leave, lock, shutdown, ...)"
#~ msgid "Actions"
#~ msgstr "Actions"

#~ msgid "Suspend"
#~ msgstr "Passer en veille"

#~ msgid "Lock/Logout Settings"
#~ msgstr "Paramètres de verrouillage / déconnexion"
